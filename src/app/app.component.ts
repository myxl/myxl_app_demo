import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { SavedLocations } from '../pages/saved-locations/saved-locations';
import { RetrieveBgImageService } from '../providers/retrieve-bg-image-service';

@Component({
  templateUrl: 'app.html',
  providers: [ RetrieveBgImageService ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  addedLocations=[];
  menuLocations = [];
  bgImageData = [];
  unitValue  = false;


  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              public menuCtrl: MenuController,
              public storage: Storage) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  onMenuItemClick(navigateTo : string){
    console.log('onMenuItemClick');

    if(navigateTo == 'savedLocations'){
      this.nav.setRoot(SavedLocations);
      this.menuCtrl.close();
    }
    else if(navigateTo == 'home'){

      this.nav.setRoot(HomePage, { preferredLocation : 'current'});
      this.menuCtrl.close();
    }
  }

  unitChanged(event : any){
    //console.log(event);
    this.storage.set('unitValue', !this.unitValue);
    this.unitValue = !this.unitValue;
  }

  menuOpened(){
    //this.manageLocations();
  }

}
