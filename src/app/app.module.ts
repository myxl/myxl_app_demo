import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { ChartsModule } from 'ng2-charts';

import { WeatherService } from '../providers/weather-service';
import { SearchService } from '../providers/search-service';
import { RetrieveBgImageService } from '../providers/retrieve-bg-image-service';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { SavedLocations } from '../pages/saved-locations/saved-locations';
import { Search } from '../pages/search/search';
import { Banner } from '../pages/banner/banner';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';

import 'rxjs/Rx';
import 'chart.js/src/chart.js';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SavedLocations,
    Search,
    Banner
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    ChartsModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SavedLocations,
    Search,
    Banner
  ],
  providers: [
    StatusBar,
    SplashScreen,
    WeatherService,
    RetrieveBgImageService,
    SearchService,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
