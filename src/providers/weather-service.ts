import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import { IWeatherData } from '../models/weather-data';
import { IChartData } from '../models/chart-data';
import { IPredictionData } from '../models/prediction-data';

import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class WeatherService {

  public baseURL: string = "http://api.openweathermap.org/data/2.5/";
  public appID: string = "80e1e409e6f62c572961baec8eedb3ec";

  constructor(private http: Http, private platform: Platform, private geolocation: Geolocation) {
    platform.ready().then(() => {
      geolocation.getCurrentPosition().then(pos => {
//        console.log("lat=" + pos.coords.latitude + "&lon=" + pos.coords.longitude);
      });
    });
  }

  getWeather(city: String, local: String, unitWind : boolean): Observable<IWeatherData> {
    var url;
    var units = 'metric';
    if(unitWind){
      units = 'imperial';
    }
    if (city == "current") {
      url = this.baseURL + "weather?" + local + "&appid=" + this.appID + "&units=" + units;
    } else {
      url = this.baseURL + "weather?q=" + city + "&appid=" + this.appID + "&units=" + units;
    }
    console.log(url);
    return this.http.get(url)
      .map((response: Response) => <IWeatherData>response.json())
//      .do(data => console.log(data))
//        .do(data => console.log(data.weather[0].main))
      
  }

  getForecast(city: String, local: String, unitWind : boolean): Observable<IChartData> {
    var url;
    var units = 'metric';
    if(unitWind){
      units = 'imperial';
    }
    if (city == "current") {
      url = this.baseURL + "forecast?" + local + "&appid=" + this.appID + "&units=" + units;
    } else {
      url = this.baseURL + "forecast?q=" + city + "&appid=" + this.appID + "&units=" + units;
    }
    return this.http.get(url)
      .map((response: Response) => <IChartData>response.json())
//      .do(data => console.log('chart data: '))
  }

  getForecastDaily(city: String, local: String, unitWind : boolean): Observable<IPredictionData> {
    var url;
    var units = 'metric';
    if(unitWind){
      units = 'imperial';
    }
    if (city == "current") {
      url = this.baseURL + "forecast/daily?" + local + "&cnt=7&appid=" + this.appID + "&units=" + units;
    } else {
      url = this.baseURL + "forecast/daily?q=" + city + "&cnt=7&appid=" + this.appID + "&units=" + units;
    }
    return this.http.get(url)
      .map((response: Response) => <IPredictionData>response.json())
//      .do(data => console.log('prediction data: '))
  }

}
