import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import { IBackgroundData } from '../models/background-data';


@Injectable()
export class RetrieveBgImageService {

  public baseURL: string = "http://ec2-34-210-13-232.us-west-2.compute.amazonaws.com:8082/";
  public imagesEndpoint: string = 'xlaxiata/countrydetails/';
  public coloursEndpoint: string = 'xlaxiata/indicator/';

  constructor(private http: Http) {

  }

  getLocationImages(): Observable<IBackgroundData> {
    return this.http.get(this.baseURL + this.imagesEndpoint)
      .map((response: Response) => <IBackgroundData>response.json())
//      .do(data => console.log(data))
  }

  getWeatherColours() {
    return this.http.get(this.baseURL + this.coloursEndpoint)
      .map((response: Response) => response.json())
//      .do(data => console.log(data))
  }
}
