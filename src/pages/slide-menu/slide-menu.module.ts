import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { SlideMenu } from './slide-menu';

@NgModule({
  declarations: [
    SlideMenu,
  ],
  imports: [
    IonicModule.forRoot(SlideMenu),
  ],
  exports: [
    SlideMenu
  ]
})
export class SlideMenuModule {}
