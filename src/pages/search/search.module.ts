import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Search } from './search';

@NgModule({
  declarations: [
    Search,
  ],
  imports: [
    IonicModule.forRoot(Search),
  ],
  exports: [
    Search
  ]
})
export class SearchModule {}
