import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SearchService } from '../../providers/search-service';

import 'rxjs/add/operator/debounceTime';

/**
 * Generated class for the Search page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class Search {
  searchTerm: string = '';
  searchControl: FormControl;
  items: any;
  searching: any = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public searchService: SearchService, public storage: Storage) {
    this.searchControl = new FormControl();
  }

  ionViewDidLoad() {
    this.setFilteredItems();
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });
  }

  onSearchInput() {
    this.searching = true;
  }

  setFilteredItems() {
    this.items = this.searchService.filterItems(this.searchTerm);
  }

  ItemSelected(selectedCity) {
    this.navCtrl.pop().then(() => this.navParams.get('resolve')(selectedCity));
    this.storage.ready().then(() => {
      this.storage.get('selectedLocations').then((val) => {
        if (val.indexOf(selectedCity) == -1) {
          val.push(selectedCity);
          console.log(val);
          this.storage.set('selectedLocations', val);
        }
      })
    });
  }

}
