import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, NavParams, Platform, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { IChartData } from '../../models/chart-data';
import { IPredictionData } from '../../models/prediction-data';
import { IPredictionDataForDisplay } from '../../models/prediction-data-for-display';
import { Search } from '../search/search';

import { WeatherService } from '../../providers/weather-service';
import { RetrieveBgImageService } from '../../providers/retrieve-bg-image-service';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [WeatherService, RetrieveBgImageService]
})
export class HomePage {
  @ViewChild(Slides) slides: Slides;

  public serviceResponse = {};
  public serviceResponseColors = {};
  public chartData: IChartData;
  public predictData: IPredictionData;
  public tempValuesArray = [, , , , , , ,];
  public tempWindArray = [, , , , ,];
  public tempWindDirectionArray = [, , , , ,];
  public predictionDataFordisplayArray: IPredictionDataForDisplay[] = [, , , , , ,];
  tempTimeArray = [, , , , , , ,];
  isChartDataAvailable: boolean = false;
  isPredictDataAvailable: boolean = false;
  addedLocations = [];
  bgImageData = [];
  bgColours = {};
  bgImageUrl: string;
  preferredLocation: string;
  locationLatLong: string;
  unitWind = false;

  constructor(
    public navCtrl: NavController,
    public weatherService: WeatherService,
    public storage: Storage,
    public retrieveBgImageService: RetrieveBgImageService,
    private platform: Platform,
    public navParams: NavParams,
    private geolocation: Geolocation,
    private alertCtrl: AlertController
  ) {
    //this.getDayFromTimestamp();
    this.preferredLocation = this.navParams.get('preferredLocation');
    if (this.preferredLocation) {
      this.manageLocations(this.preferredLocation);
    }
    else {
      this.manageLocations("Jakarta");
    }
  }

  color = '#000';

  changeBackground(): any {
    return { 'background-color': this.color };
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Location unavailable',
      subTitle: 'We are unable to get your location information. Please check your setting and retry',
      buttons: ['OK']
    });
    alert.present();
  }

  manageLocations(currentLocation: string) {
    this.storage.ready().then(() => {
      this.storage.get('selectedLocations').then((val) => {
        if (!val || !val.length) {
          if (currentLocation.toUpperCase() == 'CURRENT') {
            this.platform.ready().then(() => {
              this.geolocation.getCurrentPosition().then(pos => {
                this.locationLatLong = "lat=" + pos.coords.latitude + "&lon=" + pos.coords.longitude;
                if (this.addedLocations.indexOf(currentLocation) < 0) {
                  this.addedLocations.push(currentLocation);
                }
                this.loadData(currentLocation);
                this.setSliderData(currentLocation);
                this.getBackgroundImages(currentLocation);
              }, err => {
                this.presentAlert();
              });
            });
          }
          else {
            this.addedLocations.push(currentLocation);
            this.loadData(currentLocation);
            this.storage.set('selectedLocations', this.addedLocations);
            this.setSliderData(currentLocation);
            this.getBackgroundImages(currentLocation);
          }
        }
        else {
          this.addedLocations = val;
          if (this.addedLocations.indexOf(currentLocation) > 0) {
            this.loadData(currentLocation);
            this.setSliderData(currentLocation);
            this.getBackgroundImages(currentLocation);
          }
          else if (currentLocation.toUpperCase() == 'CURRENT') {

            this.platform.ready().then(() => {
              this.geolocation.getCurrentPosition().then(pos => {
                this.locationLatLong = "lat=" + pos.coords.latitude + "&lon=" + pos.coords.longitude;
                if (this.addedLocations.indexOf(currentLocation) < 0) {
                  this.addedLocations.push(currentLocation);
                }
                this.loadData(currentLocation);
                this.setSliderData(currentLocation);
                this.getBackgroundImages(currentLocation);
              }, err => {
                this.presentAlert();
              });
            });
          }
          else {
            this.loadData(this.addedLocations[0]);
            this.setSliderData(this.addedLocations[0]);
            this.getBackgroundImages(this.addedLocations[0]);
          }
        }
      });
      this.storage.get('selectedLocations').then((val) => {
      });
    });
  }

  setSliderData(currentLocation: string) {

    setTimeout(() => {
      var siderIndex = this.addedLocations.indexOf(currentLocation);
      this.slides.slideTo(siderIndex, 0);
    }, 1000);
  }

  getWeatherData(city) {
    this.weatherService.getWeather(city, this.locationLatLong, this.unitWind).subscribe(
      data => {
        this.serviceResponse = data;
      }
    );
  }

  getBackgroundImages(city: string) {
    this.retrieveBgImageService.getLocationImages().subscribe(data => {
      this.bgImageData = data.responseArray;
      for (var index = 0; index < this.bgImageData.length; index++) {
        if (this.bgImageData[index].city == city) {
          this.bgImageUrl = this.bgImageData[index].image;
        }
      }
    });
    this.retrieveBgImageService.getWeatherColours().subscribe(data => {
      this.bgColours = data;

      let servceData: any = this.serviceResponse;
      let currentColour: any = servceData.weather[0].main;

      let serviceResponseColours: any = this.bgColours;

      let colorValue: any = [];

      for (let data of serviceResponseColours) {
        if (data.condition == currentColour) {
          colorValue.push(data);
        }
      }

      Array.prototype.push.apply(this.serviceResponse, colorValue);

      this.serviceResponseColors = colorValue[0].color;

      function convertHex(hex, opacity) {
        hex = hex.replace('#', '');
        var r = parseInt(hex.substring(0, 2), 16);
        var g = parseInt(hex.substring(2, 4), 16);
        var b = parseInt(hex.substring(4, 6), 16);

        var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
        return result;
      }

      let rgbaResults: any = convertHex(colorValue[0].color, 75);

      this.color = rgbaResults;

    });

  }


  getChartcreationData(city: string) {
    this.weatherService.getForecast(city, this.locationLatLong, this.unitWind).subscribe(
      data => {
        this.chartData = data;
        this.isChartDataAvailable = true;
        for (let i = 0; i < 6; i++) {

          this.tempWindArray[i] = (this.chartData.list[i].wind.speed);
          this.tempWindDirectionArray[i] = (this.chartData.list[i].wind.deg);

        }
        for (let i = 0; i < 8; i++) {
          this.tempValuesArray[i] = (this.chartData.list[i].main.temp);
          // this.tempWindArray[i] = (this.chartData.list[i].wind.speed);
          var time = this.chartData.list[i].dt_txt;
          var hour = (time.split(' '))[1];
          var hourin24hours = hour.split(':')[0];
          this.tempTimeArray[i] = (hourin24hours);
        }
        this.lineChartData = this.lineChartData.slice();
      }
    );
  }

  getPredictionData(city: string) {
    this.weatherService.getForecastDaily(city, this.locationLatLong, this.unitWind).subscribe(
      data => {
        this.predictData = data;
        this.isPredictDataAvailable = true;
        if (this.predictData.list) {
          for (let i = 0; i < this.predictData.list.length; i++) {
            let displayObj: IPredictionDataForDisplay = {
              "min": this.predictData.list[i].temp.min,
              "max": this.predictData.list[i].temp.max,
              "icon": ("http://openweathermap.org/img/w/".concat(this.predictData.list[i].weather[0].icon)).concat(".png"),
              "day": this.getDayFromTimestamp(this.predictData.list[i].dt)
            };
            this.predictionDataFordisplayArray[i] = (displayObj);
          }
        }
      }
    );
  }


  getDayFromTimestamp(ts: number): string {
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    var currentTime = new Date(ts * 1000);
    var day = weekday[currentTime.getDay()];
    return day;

  }
  // unitWind = false;

  loadData(city) {

    this.storage.get('unitValue').then((val) => {
      if (val != null) {
        this.unitWind = val;
      }
      this.getWeatherData(city);
      this.getChartcreationData(city);
      this.getPredictionData(city);
      this.getBackgroundImages(city);
    });
  }

  addPage() {
    new Promise((resolve, reject) => {
      this.navCtrl.push(Search, { resolve: resolve });
    }).then(data => {
      //this.addedLocations.push(data);
      if (data) {
        this.manageLocations(data.toString());
      }
    });
  }

  public lineChartData: Array<any> = [
    {
      label: 'TEMPERATURE',
      data: this.tempValuesArray
    }
  ];


  public lineChartLabels: Array<any> = this.tempTimeArray;

  public lineChartOptions: any = {
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        ticks: {
          fontColor: '#fff',
          fontSize: 18
        },
        gridLines: {
          color: 'rgba(255,255,255,1)',
          lineWidth: 0.5
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: '#fff',
          fontSize: 18
        },
        gridLines: {
          color: 'rgba(255,255,255,1)',
          lineWidth: 0.5
        }
      }]
    },
    responsive: true
  };

  public lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(255,255,255,0.2)',
      borderColor: 'rgba(255,255,255,1)',
      pointBackgroundColor: 'rgba(255,255,255,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255,255,255,0.8)'
    },

  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    //console.log("Current index is", currentIndex);
    if (this.addedLocations.length > currentIndex) {
      this.loadData(this.addedLocations[currentIndex]);
    }
  }

  mySlideOptions = {
    pager: true,
    paginationClickable: true,
    pagination: '.swiper-pagination',
    onReachEnd: function () {
      this.showButton = true;
    }
  };


}
