import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'page-saved-locations',
  templateUrl: 'saved-locations.html'
})
export class SavedLocations {
  items: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    storage.ready().then(() => {
      storage.get('selectedLocations').then((val) => {
        this.items = val;
      })
    });


  }

  deleteItem(item : string) {
    var index = this.items.indexOf(item);
    if (index > -1) {
      this.items.splice(index, 1);
    }
    this.storage.ready().then(() => {
      this.storage.set('selectedLocations', this.items);
    });
    console.log(this.items);
  }

  selectLocation(item :string){
    this.storage.ready().then(() => {
      this.storage.set('preferredLocation', item);
      this.navCtrl.setRoot(HomePage, {preferredLocation: item});
    });
  }
}

