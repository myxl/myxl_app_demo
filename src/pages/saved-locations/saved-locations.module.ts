import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { SavedLocations } from './saved-locations';

@NgModule({
  declarations: [
    SavedLocations,
  ],
  imports: [
    IonicModule.forRoot(SavedLocations),
  ],
  exports: [
    SavedLocations
  ]
})
export class SavedLocationsModule {}
