import { Component } from '@angular/core';

/**
 * Generated class for the Banner page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'add-banner',
  templateUrl: 'banner.html',
})

export class Banner {
  fullPath: string;
  closeBanner: any;
  DisplayClass: string;
  imageUrl: string;
  constructor() {
    this.DisplayClass = 'visible';
    this.imageUrl = "AddBanner.jpeg";
    this.fullPath = "../../assets/" + this.imageUrl;
    this.closeBanner = () => {
      this.DisplayClass = 'hidden'
    }
  }
}
