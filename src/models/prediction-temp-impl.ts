import { IPredictionTemp } from './prediction-temp';

export class PredictionTemp implements IPredictionTemp {
   day: number
    min: number
    max: number
    night: number
    eve : number
    morn : number

}