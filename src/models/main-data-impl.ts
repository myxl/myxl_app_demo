import { IMainData } from './main-data';

export class MainData implements IMainData {
    temp: number
    pressure: number
    humidity: number
    temp_min: number
    temp_max: number

}