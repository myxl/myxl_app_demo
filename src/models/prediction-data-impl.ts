import { IPredictionData } from './prediction-data';
import { IPredictionList } from './prediction-list';

export class PredictionData implements IPredictionData {
    city: any
    cod: string
    message: number
    cnt: number
    list: IPredictionList[]

}