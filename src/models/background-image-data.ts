export interface IBackgroundImageData {
  city: string,
  image: string,
  description: string
}
