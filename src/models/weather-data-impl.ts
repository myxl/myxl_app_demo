import { Component } from '@angular/core'

import { IWeatherData } from './weather-data';
import { IMainData } from './main-data';
import { IWeatherobj } from './weatherobj';
import { IWind } from './wind';
import { IClouds } from './clouds';

@Component({
    selector: 'pm-weatherdata',
})

export class WeatherData implements IWeatherData {
    coord : any
    weather : IWeatherobj[]
    base : string
    main : IMainData
    visibility : number
    wind : IWind
    clouds : IClouds
    dt : number
    id : number
    name : string
    cod : number

}
