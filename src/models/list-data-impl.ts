import { IListData } from './list-data';
import { IWeatherobj } from './weatherobj';
import { IClouds } from './clouds';
import { IWind } from './wind';
import { IChartMainData } from './chart-main-data';

export class ListData implements IListData {
    dt: string
    main : IChartMainData
    weather : IWeatherobj[]
    clouds : IClouds 
    wind : IWind
    rain : any
    sys : any
    dt_txt : string
}