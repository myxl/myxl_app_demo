import { IPredictionList } from './prediction-list';

export interface IPredictionData {
    city: any
    cod: string
    message: number
    cnt: number
    list: IPredictionList[]

}