import { IBackgroundImageData } from './background-image-data';

export interface IBackgroundData {
  responseArray : IBackgroundImageData[]
}
