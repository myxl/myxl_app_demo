import { IPredictionList } from './prediction-list';
import { IWeatherobj } from './weatherobj'
import { IPredictionTemp } from './prediction-temp';

export interface IPredictionList {
    dt: number
    temp: IPredictionTemp
    pressure: number
    humidity: number
    weather: IWeatherobj
    speed : number
    deg : number
    clouds : number
    rain : number

}