import { IPredictionDataForDisplay } from './prediction-data-for-display';

export class PredictionDataForDisplay implements IPredictionDataForDisplay {
    min: number
    max: number
    icon: string
    day: string
}