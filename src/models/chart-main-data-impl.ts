import { IChartMainData } from './chart-main-data';

export class ChartMainData implements IChartMainData {
    temp: string
    temp_min : number
    temp_max : number
    pressure : number 
    sea_level : number
    grnd_level : number
    humidity : number
    temp_kf : number
}