export interface IPredictionDataForDisplay {
    min: number,
    max: number,
    icon: string,
    day : string
}