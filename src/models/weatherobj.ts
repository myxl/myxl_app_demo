export interface IWeatherobj {
    id: number,
    main: string,
    description: string,
    icon: string

}