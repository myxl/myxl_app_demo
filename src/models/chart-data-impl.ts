import { IChartData } from './chart-data';
import { IListData } from './list-data';

export class ChartData implements IChartData {
    cod: string
    message : number
    cnt : number
    list : IListData[] 
    city : any
}