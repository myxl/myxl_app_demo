import { IListData } from './list-data';

export interface IChartData {
    cod: string
    message : number
    cnt : number
    list : IListData[]
    city : any
}